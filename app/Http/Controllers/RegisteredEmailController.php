<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\RegisteredEmail;
use App\mailService;

class RegisteredEmailController extends Controller
{

    public function __construct (mailService $mailService) {
        $this->mailService = $mailService;
    }

    public function checkToken($token) {
        $user = RegisteredEmail::where('token', $token)->first();

        if($user instanceof RegisteredEmail) {
            switch ($user->emailaddress) {
                case 'sharon.meeus@gmail.com':
                    return response()->json(["status" => "success"]);
                
                default:
                    return response()->json(["error" => "Geen bandlid"], 404);
            }
        } else {
            return response()->json(["error" => "Geen user"], 404);
        }
    }

    public function store(Request $request)
    {
        $user = RegisteredEmail::where('emailaddress', $request->email)->get();

        if(count($user) < 1) {

            $email = new RegisteredEmail;

            $email->emailaddress = $request->email;

            do{
                $token = hash_hmac('sha256', str_random(40), 'secret');
            } while (RegisteredEmail::where("token", "=", $token)->first() instanceof RegisteredEmail);

            $email->token = $token;

            $email->save();

            $this->mailService->sendRegistrationMail($email);

            return response()->json(['status' => 'success', 'user' => $user]);
        } else {
            return response()->json(['error' => 'Dit emailadres is al ingeschreven', 'user' => $user], 400);
        }
        
    }

    public function destroy($token) 
    {
        $registered = RegisteredEmail::where('token', $token)->first();
        
        if($registered instanceof RegisteredEmail) {
            $registered->delete();
        } else {
            return response()->json(['error' => 'Dit emailadres is niet ingeschreven.'], 404);
        }
        

        return response()->json(['status' => 'success']);
    }

}
