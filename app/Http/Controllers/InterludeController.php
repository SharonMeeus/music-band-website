<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interlude;
use App\Playlist;

use Carbon\Carbon;

class InterludeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $break = new Interlude;
        $break->after_position = $request->position;
        $break->length = $request->minutes;
        $break->playlist_id = $request->playlist;

        $break->save();

        $playlist = Playlist::find($request->playlist);
        $playlist->updated_at = Carbon::now();
        $playlist->save();

        return response()->json(["status" => "success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $break = Interlude::find($id);

        $playlist = Playlist::find($break->playlist_id);
        $playlist->updated_at = Carbon::now();
        $playlist->save();

        $break->delete();
    }

    public function removeBreaksByPlaylist($id) {

        $breaks = Interlude::where('playlist_id', $id)->get();

        foreach ($breaks as $break) {
            $break->delete();
        }

        return response()->json(["status" => "success"]);
    }
}
