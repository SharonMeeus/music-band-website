<?php

namespace App\Http\Controllers;

use App\LatestEvent;
use Illuminate\Http\Request;

class LatestEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = LatestEvent::all();

        return response()->json(["event" => $events]);
    }

    public function getLastEvent() 
    {
        $event = LatestEvent::all();
        $event = $event->orderBy('date', 'desc')->last();

        return response()->json(["event" => $event]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new LatestEvent;

        $event->title = $request->event_title;
        $event->date = $request->event_date;

        $event->save();

        return response()->json(['status' => 'success']);
    }

}
