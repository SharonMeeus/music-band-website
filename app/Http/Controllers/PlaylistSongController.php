<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\PlaylistSong;
use App\Songposition;
use App\Playlist;
use App\Interlude;
use Carbon\Carbon;

class PlaylistSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = PlaylistSong::all();
        return response()->json(["songs" => $songs]);
    }

    public function newestPlaylist() 
    {
        $playlists = Playlist::all();
        $playlist = $playlists->sortByDesc('updated_at')->values()->first();

        $filled_playlist = $this->getSongs($playlist);

        $filled_playlist->breaks = $this->getBreaks($playlist->id);
        
        return response()->json(["playlist" => $filled_playlist]);
    }

    public function PlaylistById ($playlist_id) {

        $playlist = Playlist::find($playlist_id);

        $filled_playlist = $this->getSongs($playlist);

        $filled_playlist->breaks = $this->getBreaks($playlist->id);
        
        return response()->json(["playlist" => $filled_playlist]);
    }

    public function getSongs ($playlist) {

        $songs = PlaylistSong::all();
        $songs_inside = [];
        $songs_outside = [];
        $songpositions = Songposition::where('playlist_id', $playlist->id)->get();

        foreach ($songpositions as $songposition) {

            $newsong = PlaylistSong::where('id', $songposition->playlistsong_id)->get();

            $newsong[0]->position = $songposition->position;

            array_push($songs_inside, $newsong[0]);

            foreach ($songs as $key => $song) {

                if($song->id == $songposition->playlistsong_id) {
                    $songs->pull($key);
                }

            }

        }

        foreach ($songs as $song) {
            array_push($songs_outside, $song);
        }

        $playlist->songs_inside = $songs_inside;
        $playlist->songs_outside = $songs_outside;

        return $playlist;
    } 

    public function getBreaks ($playlist_id) {

        $breaks = Interlude::where('playlist_id', $playlist_id)->get();
        $mybreaks = $breaks->sortBy('after_position');
        return $mybreaks->values()->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function savePlaylist(Request $request) {

        $songs = (object)$request->songs;
        $playlist = (object)$request->playlist;

        $playlistsongs = $playlist->songs_inside;
        $songs_to_remove = [];

        foreach ($songs as $song) {

            $song = (object)$song;
            $where_arr = ["playlist_id" => $playlist->id, "playlistsong_id" => $song->id];

            $song_to_remove = Songposition::where($where_arr)->first();

            if($song_to_remove["id"]) {
                Songposition::where("id", $song_to_remove["id"])->delete();
            }
        }

        foreach ($playlistsongs as $playlistsong) {

            $playlistsong = (object)$playlistsong;
            $song_to_edit = Songposition::where('playlist_id', $playlist->id)->where('playlistsong_id', $playlistsong->id)->first();

            if($song_to_edit) {
                $song_to_edit->position = $playlistsong->position;

                $song_to_edit->save();
            } else {

                $new_song_position = new Songposition;
                $new_song_position->playlist_id = $playlist->id;
                $new_song_position->playlistsong_id = $playlistsong->id;
                $new_song_position->position = $playlistsong->position;

                $new_song_position->save();
            }
        }

        $edited_playlist = Playlist::find($playlist->id);
        $edited_playlist->updated_at = Carbon::now();

        $edited_playlist->save();

        return response()->json(["status" => "success"]);
    }

    public function emptyPlaylist ($id) {

        $songs = Songposition::all();

        $breaks = Interlude::all();

        foreach($songs as $song) {

            if($song->playlist_id == $id) {
                $song->delete();
            }
        }

        foreach ($breaks as $break) {
            if($break->playlist_id == $id) {
                $break->delete();
            }
        }

        return response()->json(["status" => "success"]);
    }

    public function update(Request $request, $id)
    {
        $song = PlaylistSong::find($id);
        $song->artist = $request->artist;
        $song->title = $request->title;
        $song->length = $request->length;

        $song->save();

        return response()->json(["status" => "success"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
