<?php

namespace App\Http\Controllers;

use App\FacebookNumber;
use Illuminate\Http\Request;

class FacebookNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FacebookNumber::all();
        return response()->json(["fb_data" => $data]);
    }

   
}
