<?php

namespace App\Http\Controllers;

use App\Playlist;
use App\Songposition;
use App\Interlude;

use Illuminate\Http\Request;
use Carbon\Carbon;

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $playlists = Playlist::all();
        return response()->json(["playlists" => $playlists]);
    }

    public function getPlaylistById ($id) {

        $playlist = Playlist::findOrFail($id);
        $songs = $playlist->playlist_songs()->get();

        return response()->json(["playlist" => $playlist, "songs" => $songs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $playlist = new Playlist;

        $playlist->name = $request->name;
        $playlist->save();

        return response()->json(["status" => "success", "playlist" => $request]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deletePlaylist ($id) 
    {
        $songpositions = Songposition::all();
        $breaks = Interlude::all();

        foreach($songpositions as $songposition) {

            if ($songposition->playlist_id == $id) {
                Songposition::find($songposition->id)->delete();
            }
        } 

        foreach ($breaks as $break) {
            if ($break->playlist_id == $id) {
                Interlude::find($break->id)->delete();
            }
        }

        $playlist = Playlist::find($id);
        $playlist->delete();
    }

    public function destroy($id)
    {
        //
    }
}
