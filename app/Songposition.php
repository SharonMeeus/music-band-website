<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Songposition extends Model
{
	public $timestamps = false;
	
    public function playlist_songs () {
    	return $this->belongsToMany('App\PlaylistSong');
    }

    public function playlists () {
    	return $this->belongsToMany('App\Playlist');
    }
}
