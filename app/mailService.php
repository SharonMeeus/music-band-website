<?php
namespace App;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class mailService
{
	protected $mailer;

    public function __construct (Mailer $mailer) {
    	$this->mailer = $mailer;
    }

    public function sendRegistrationMail($user)
    {
        $link = "www.tebs.be/#/unsubscribe/" . $user->token;
        $this->mailer->send('mails.registrationmail',['link' => $link], function 
            (Message $m) use ($user) {
                $m->to($user->emailaddress)
                  ->from("music@tebs.be")
                  ->subject('Inschrijving evenementen TEBS');
        });
 
    }
}