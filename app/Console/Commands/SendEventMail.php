<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LatestEvent;
use App\mailService;
use App\RegisteredEmail;

class SendEventMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email when new event detected';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(mailService $mailService)
    {
        parent::__construct();
        $this->mailService = $mailService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.facebook.com/v2.7/theearlybirdspecials/events?access_token=1908190316060720|033635a28c0a95dd44154b4db61caec2',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true); //because of true, it's in an array
        $lastfbevent = $response["data"][0]["start_time"];

        $lastdbevent = LatestEvent::all()->first();

        if($lastfbevent > $lastdbevent) {
            
        }*/
        echo "sending mail \n";
        $token = hash_hmac('sha256', str_random(40), 'secret');
        $user = new RegisteredEmail();
        $user->emailaddress = 'sharon.meeus@gmail.com';
        $user->token = hash_hmac('sha256', str_random(40), 'secret');
        $this->mailService->sendRegistrationMail($user);
        echo "mail sent \n";
    }
}
