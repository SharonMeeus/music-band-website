<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistSong extends Model
{
    public $timestamps = false;

    public function playlists () {

    	return $this->belongsToMany('App/Playlist');
    }

    public function songpositions () {

    	return $this->hasMany('App\Songposition');
    }
}
