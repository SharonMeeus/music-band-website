<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    public function playlist_songs () {
    	
    	return $this->belongsToMany('App\PlaylistSong');
    }

    public function songpositions () {

    	return $this->hasMany('App\Songposition');
    }

}
