<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interlude extends Model
{
	public $timestamps = false;
	
    public function playlist () {
    	$this->belongsTo('App\Playlist');
    }
}
