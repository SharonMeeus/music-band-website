<!DOCTYPE HTML>
<html ng-app="tebs-app" ng-cloak>
<head>
	<meta charset="utf-8" />
	<meta name="description" content="Officiële website muziekgroep TEBS, The Early Bird Specials">
	<meta name="keywords" content="tebs, specials, muziek, jazz">
	<meta name="author" content="Sharon Meeus">
    <meta name="format-detection" content="telephone=no" />
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1" />
    <meta property="og:image" content="http://www.tebs.be/img/thumb.jpg" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/helpers.css">
    <link rel="stylesheet" type="text/css" href="dist/css/magnific-popup.css">

	<title>T.E.B.S. - The Early Bird Specials</title>
</head>
<body>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1908190316060720',
                xfbml      : true,
                version    : 'v2.9'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    
	<tebs-header></tebs-header>

	<div ui-view ng-cloak></div>

    <script src="https://code.jquery.com/jquery-1.12.1.min.js" integrity="sha256-I1nTg78tSrZev3kjvfdM5A5Ak/blglGzlaZANLPDl3I=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.js"></script>
    <script src="https://code.createjs.com/preloadjs-0.6.2.min.js"></script>
    
    <script type="text/javascript" src="assets/js/angular-materialize.js"></script>
    <script type="text/javascript" src="assets/js/angular-touch.js"></script>

	<script type="text/javascript" src="app/app.js"></script>
    <script type="text/javascript" src="app/config.js"></script>
    <script type="text/javascript" src="app/routes.js"></script>
    <script type="text/javascript" src="app/util.js"></script>

    <script type="text/javascript" src="app/controllers/homeController.js"></script>
    <script type="text/javascript" src="app/controllers/eventsController.js"></script>
    <script type="text/javascript" src="app/controllers/aboutController.js"></script>
    <script type="text/javascript" src="app/controllers/contactController.js"></script>
    <script type="text/javascript" src="app/controllers/galleryController.js"></script>
    <script type="text/javascript" src="app/controllers/musicController.js"></script>
    <script type="text/javascript" src="app/controllers/playlistController.js"></script>
    <script type="text/javascript" src="app/controllers/unsubscribeController.js"></script>

    <script type="text/javascript" src="app/directives/header/tebs-header.js"></script>
    <script type="text/javascript" src="app/directives/magnific-popup/magnific-popup.js"></script>
    <script type="text/javascript" src="app/directives/loader/loading-sign.js"></script>

    <script type="text/javascript" src="app/services/emailService.js"></script>
    <script type="text/javascript" src="app/services/eventService.js"></script>
    <script type="text/javascript" src="app/services/facebookService.js"></script>
    <script type="text/javascript" src="app/services/photoService.js"></script>
    <script type="text/javascript" src="app/services/playlistService.js"></script>

    <script type="text/javascript" src="dist/js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="dist/js/smoothscroll.js"></script>

</body>
</html>