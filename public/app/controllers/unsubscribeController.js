app.controller("unsubscribeController", function (emailService, $stateParams) {

	var vm = this;
	var emailSvc = emailService;

	function deleteEmail() {
		emailSvc.deleteRegisteredEmails($stateParams.token).then(function (data) {
			console.log(data);

			$(".unsubscribecontent").fadeIn();
		}, function (error) {

			if(error.data.error) {
				swal(
					'<h2 class="Lora">Oeps...</h2>',
					error.data.error + '<br/> Schrijf je in via de <a onclick="swal.closeModal();" href="#/evenementen">evenementenpagina</a>',
					'error'
				);
			} else {
				swal({
					title: 'Oeps...',
					html: 'We konden je emailadres niet uitschrijven. <br/> Probeer nog eens contacteer ons via <a href="mailto:music@tebs.be">music[at]tebs.be</a>',
					type:'error',
					target: '.unsubscribepage'
				});
			}
		});
	}

	function _init() {
		$(".unsubscribecontent").hide();
		deleteEmail();

	}

	_init();
});