app.controller("homeController", function (photoService) {

	var vm = this;

	var photoSvc = photoService;
	var page = ".home";

	// scroll plugin on arrow click

	function getPhoto () {
		photoSvc.getPhotosByPage("home").then(function (data) {

			var photo_arr = [];

			for(x in data.data.photos) 
			{
				photo_arr.push(data.data.photos[x].src);
			}

			console.log(photo_arr);
			handleLoading(page, photo_arr);

		}, function (error) {
			console.log(error);
			var photo_arr = ['img/home/BG.jpg'];
			handleLoading(page, photo_arr);
		})
	}

	vm.scrollDown = function () {

		var height = $(".home-bg").height();
		window.scroll({ top: height, left: 0, behavior: 'smooth' });
	}

	function _init() {
		$(page).hide();
		getPhoto();
	}

	_init();
});