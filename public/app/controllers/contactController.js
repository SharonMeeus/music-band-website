app.controller("contactController", function (photoService, $http, $location) {

	var vm = this;
	vm.formData = {};

	var photoSvc = photoService;

	var page = ".contact-bg";

	function getPhoto () {
		photoSvc.getPhotosByPage("contact").then(function (data) {

			var photo_arr = [];

			for(x in data.data.photos) 
			{
				photo_arr.push(data.data.photos[x].src);
			}

			console.log(photo_arr);
			handleLoading(page, photo_arr);

		}, function (error) {
			console.log(error);
			var photo_arr = ['img/contact/img5.jpg'];
			handleLoading(page, photo_arr);
		})
	}

	//Send email

	vm.sendForm = function() {

		//send form to send.php
		if(vm.formData.name && vm.formData.email && vm.formData.message) {
			console.log(vm.formData);
			$http({
				method: 'POST',
                url: 'app/pages/send.php',
                data: $.param(vm.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' } 
			})
			// if email was sent
			.then(function(data){
				console.log(data);
				swal({
					title: '<h2 class="Lora">Super!<h2>',
					text: 'We hebben je berichtje aangekregen en laten je snel iets weten.',
					type: 'success'
				})
			//if error
			}, function(data){
				console.log(data);
				swal(
					'<h2 class="Lora">Helaas...<h2>',
					'Er ging iets mis bij het versturen. We proberen hier zo snel mogelijk iets aan te doen',
					'error'
				)

			});
		} 
	}

	function _init() {

		$(page).hide();
		
		getPhoto();
	}

	_init();
});