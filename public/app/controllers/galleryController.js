app.controller("galleryController", function (photoService) {

	var vm = this;
	vm.photo_arr = [];

	var photoSvc = photoService;

	var photosrc_arr = [];
	var loader = ".galleryloader";
	var page = ".gallerypage";
	

	function getPhotos () {

		photoSvc.getPhotosByPage("photo's").then(function (data) {

			for(x in data.data.photos) 
			{
				vm.photo_arr.push(data.data.photos[x]);
				photosrc_arr.push(data.data.photos[x].src)
			}

			console.log(vm.photo_arr);
			console.log(photosrc_arr);
			handleLoading(page, photosrc_arr);

		}, function (error) {
			console.log(error)
			swal("<h2 class='Lora'>Oei</h2>", "Door problemen met de server kunnen we deze pagina even niet laden. Probeer later opnieuw.", "error").then(function() {
				window.location.href = "#";
			});
		});
	}

	function _init() {

		$(page).hide();
		getPhotos();
	}

	_init();
});