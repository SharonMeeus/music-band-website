app.controller("musicController", function (playlistService) {

	var vm = this;
	vm.playlist = [];

	var playlistSvc = playlistService;
	var page = ".musicpage";
	// array of songs for playlist (ng-repeat)
	

	function getPlaylist () {
		playlistSvc.getSongs().then(function (data) {

			console.log(data.data.songs);
			var playlist = data.data.songs;

			for(x in playlist) {
				if(playlist[x].title.indexOf("Medley") < 0) {
					vm.playlist.push(playlist[x]);
				}
			}

			var findLoadingbar = setInterval(function () {

				if($(".loading-bar")) {

					$(".loading-bar").animate({"width": "100vw"}, 50, function() {
						
						$(".loader").fadeOut();
						$(page).fadeIn();
					});

					clearInterval(findLoadingbar);
				}

			}, 50)

		}, function (error) {
			console.log(error);
			swal("<h2 class='Lora'>Oei</h2>", "Door problemen met de server kunnen we deze pagina even niet laden. Probeer later opnieuw.", "error").then(function() {
				window.location.href = "#";
			});
		});
	}

	function _init() {
		$(page).hide();
		getPlaylist();
	}

	_init();
});