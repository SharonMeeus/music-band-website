app.controller("eventsController", function (facebookService, emailService, $scope) {

	var vm = this;

	var facebookSvc = facebookService;
	var emailSvc = emailService;

	var google_key = "AIzaSyDBT2vHwdWduRrnjVPEQEnoJy0V-e25dKE";
	vm.fb_events = [];

	function getFacebookEvents () {


		var fbinterval = setInterval(function () {

			if(FB) {
				clearInterval(fbinterval);
				facebookSvc.getFacebookData().then(function (data) {

					var fb_o = data.data.fb_data[0];

					FB.api(
				  		'/theearlybirdspecials/events?access_token=' + fb_o.app_id +'|' + fb_o.app_secret,
				  		'GET',
				  		{},
				  		function(response) {

				  			var six_months = 300 * 24 * 60 * 60 * 1000;
				  			var today = new Date();

				  			for(i in response.data) {
				  				if(today - new Date(response.data[i].start_time.slice(0, 19)) < six_months) {
				  					vm.fb_events.push(response.data[i]);
				  				}
				  			}
				  			formatEvents();
				  		}
					);
				}, function (error) {
					console.log(error);
					swal("<h2 class='Lora'>Oei</h2>", "Door problemen met de server kunnen we deze pagina even niet laden. Probeer later opnieuw.", "error").then(function() {
				window.location.href = "#";
			});
				})
				
			}
		}, 100);

	}

	function formatEvents () {
		for(x in vm.fb_events) {
			
			var fb_event = vm.fb_events[x];
			var event_name = fb_event.name.toLowerCase();
			var event_location = "";

			/* Make private or not */
			if(event_name.indexOf("priv") >= 0) {
				fb_event.private_event = true;
			} else {
				fb_event.private_event = false;
			}

			/* set long time and date */ 
			var fbdate = fb_event.start_time.slice(0, 19);
			fbdate = fbdate.replace(/\-/g, '/');
			fbdate = fbdate.replace("T", " ");
			var longdate = new Date(fbdate).toLocaleTimeString("nl-NL", { weekday: "long", year: "numeric", month: "long", day: "numeric" });
			fb_event.time_and_date = longdate.slice(0, -13) + "om " + longdate.slice(-8, -6) + "u" + longdate.slice(-5, -3);

			/* set short day and date */
			fb_event.shortday = createShortDay(new Date(fbdate));
			fb_event.shortdate = createShortDate(new Date(fbdate));

			/* set address*/
			fb_event.rsvp_url = "https://www.facebook.com/events/" + vm.fb_events[x].id;

			if(fb_event.place.location) {
				event_location = fb_event.place.location;

				if(event_location.street) {
					fb_event.address = event_location.street + ", " + event_location.zip + " " + event_location.city + " (" + fb_event.place.name + ")"; 
				} else if (event_location.zip) {
					fb_event.address = event_location.zip + " " + event_location.city;
				} else {
					fb_event.address = event_location.city;
				}
			} else {
				fb_event.address = fb_event.place.name;
			}

			/* set google address */
			if(event_name.indexOf("surprise") >= 0 || event_name.indexOf("verrassing") >= 0) {
				fb_event.address_url = null;
			} else if (event_location != "") {
				if(event_location.street) {
					fb_event.address_url = "https://www.google.com/maps/embed/v1/place?key=" + google_key + "&q=" + event_location.street + "," + event_location.zip + "," + event_location.city + ',' + fb_event.place.name;
				} else if (event_location.zip) {
					fb_event.address_url = "https://www.google.com/maps/embed/v1/place?key=" + google_key + "&q=" + event_location.zip + "," + event_location.city;
				} else {
					fb_event.address_url = "https://www.google.com/maps/embed/v1/place?key=" + google_key + "&q=" + event_location.city;
				}
				
			} else {
				fb_event.address_url = "https://www.google.com/maps/embed/v1/place?key=" + google_key + "&q=" + fb_event.place.name
			}

		} 

		console.log(vm.fb_events);
		$scope.$apply(vm.fb_events);

		var findLoadingbar = setInterval(function () {

			if($(".loading-bar")) {

				$(".loading-bar").animate({"width": "100vw"}, 50, function() {
					
					$(".loader").fadeOut();
					$(".events-wrapper").fadeIn();
				});

				clearInterval(findLoadingbar);
			}

		}, 50)
	}

	function createShortDay (date) {

		day_num = date.getDay();

		switch(day_num) {
			case 0:
				return "ZON";
			case 1:
				return "MA";
			case 2:
				return "DIN";
			case 3:
				return "WOE";
			case 4:
				return "DON";
			case 5:
				return "VRY";
			case 6:
				return "ZAT";

		}
	}

	function createShortDate (date) {
		var newdate = date.toLocaleDateString("nl-NL", { weekday: "short", year: "numeric", month: "short", day: "numeric" }).slice(3, 16).replace(".", "");

		return newdate;
	}

	// Check if an event has passed, if so: return true
	vm.checkIfPassedEvent = function(date) {

		//Make sure the date is converted to Date-format
		var eventdate = new Date(date);
		var currentdate = new Date();

		//Compare dates
		if (eventdate >= currentdate) {
			return false;
		} else {
			return true;
		}
	}

	//lightbox plugin
	function initializeLightbox () {
		// first wait for element(s) 
		var mapsInterval = setInterval(function () {

			if($('.google-map').length) {

				$('.google-map').magnificPopup({
		            type:'iframe',
		            
		            iframe: {

		                gmaps: {

		                  index: '//maps.google.',
		                  src: 'https://www.google.com/maps/embed/v1/place?%id%'
		                }
		            }
		            
		        });
			// No endless loops wanted	
			} else {

        		setTimeout(function () {clearInterval(mapsInterval)}, 2000);
        	}

		}, 100);

	}

	//collapsible plugin
	function initializeCollapsibles () {

		//collapsible table initialisation
		$('.collapsible').collapsible({
	      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
	    });
	}

	//disable events after .passed-event was applied
	function disableEvents () {

		// first wait for element(s) 
		var passedEventInterval = setInterval(function() {
        	
        	if($(".passed-event").length) {

        		$(".passed-event").each(function () {
			
					$(this).click(function (e) {

						e.stopPropagation();
						Materialize.toast("Dit evenement is al voorbij...", 3000, "tertiary-colour");
					});
				});


        		clearInterval(passedEventInterval);
        	// No endless loops wanted
        	} else {

        		setTimeout(function () {clearInterval(passedEventInterval)}, 2000);
        	}

        }, 100);
	}

	vm.startRegistration = function () {

		swal({
			title: '<h5 class="Lora">Geen evenement meer missen? Schrijf je hier in!</h5>',
			input: 'email',
			showCancelButton: true,
			confirmButtonText: 'Schrijf me in',
			cancelButtonText: 'Sorry, toch maar niet',
			showLoaderOnConfirm: true,
			preConfirm: function (emailaddress) {
				return new Promise(function (resolve, reject) {
					emailSvc.NewRegisteredEmail({email: emailaddress}).then(function (data) {
						console.log(data);
						resolve();
					}, function (error) {
						if(error.data.error) {
							reject(error.data.error);
						} else {
							reject("Door een probleempje met onze server konden we je niet inschrijven...");
						}
					});
				})
			},
			allowOutsideClick: true
			}).then(function (email) {
			swal({
			type: 'success',
			title: '<h5 class="Lora">We hebben je ingeschreven!</h5>',
			html: 'Er is een bevestiging gestuurd naar:' + email + '. <br/> <span class="smalltext">*Check even je spam als je onze email niet in je mailbox kan vinden.</span>'
			})
			})
	}






	function _init() {

		$(".events-wrapper").hide();
		getFacebookEvents();
		initializeLightbox();
		disableEvents();
		initializeCollapsibles();

      	Materialize.toast('Klik op de titel van een evenement om meer info te zien', 5000); 
	}

	

	_init();
});