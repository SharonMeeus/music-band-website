app.controller("aboutController", function (photoService) {

	var vm = this;
	vm.photo_arr = [];
	vm.person_arr = [
		{ name: "Bart Lenaerts",
		  instrument: "Saxofoon"}, 
		{ name: "Franky Thijs",
		  instrument: "Drums" }, 
		{ name: "Geert Geudens",
		  instrument: "Bas"}, 
		{ name: "Isabelle Hoogmartens",
		  instrument: "Zang"}, 
		{ name: "Kristof Leten",
		  instrument: "Gitaar"}, 
		{ name: "Sharon Meeus",
		  instrument: "Zang & Keys"}
	];

	var photoSvc = photoService;

	var page = ".aboutpage";

	var photosrc_arr = [];

	function getPhotos () {

		photoSvc.getPhotosByPage('about').then(function (data) {

			for(x in data.data.photos) 
			{
				photosrc_arr.push(data.data.photos[x].src);
				vm.photo_arr.push(data.data.photos[x]);
				
			}

			handleLoading(page, photosrc_arr);

		}, function (error) {	

			console.log(error);
			swal("<h2 class='Lora'>Oei</h2>", "Door problemen met de server kunnen we deze pagina even niet laden. Probeer later opnieuw.", "error").then(function() {
				window.location.href = "#";
			});
		});


	}

	function _init() {

		//parallax plugin
      	$('.parallax').parallax();
      	$(page).hide();
      	getPhotos();
		
	}

	_init();
});