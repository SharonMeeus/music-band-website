app.controller("playlistController", function ($stateParams, $state, $scope, $compile, emailService, playlistService) {

	var vm = this;
	vm.allplaylists = [];
	vm.current_playlist = [];
	vm.songs_outside_pl = [];
	vm.songs_inside_pl = [];
	vm.selected = 'Nummers';

	var pl_breaks = [];
	var emailSvc = emailService;
	var playlistSvc = playlistService;

	var isAuthorized = false;
	var changeState = false;
	var playlistHasChanged = false;
	var showSidebar = false;
	var swalmessage = [];
	var playlist_is_empty = false;

	var page = ".playlistpage";


	function toggleLoadingSign (bl_show) {

		var myinterval = setInterval(function () {

			if($(".loader")) {
				if(bl_show) {
					$(".loader").show();
				} else {
					$(".loader").hide();
				}

				clearInterval(myinterval);
			}
		}, 50);
	}

	function checkToken (callback, cb_array) {

		var token = $stateParams.token;

		emailSvc.checkToken(token).then(function (data) {

			isAuthorized = true;
			toggleLoadingSign(true);
			$(".loading-bar").animate({"width": "50vw"}, 500);

			checkCallback(callback, cb_array);

		}, function (error) {

			swal({
				title: '<h3 class="Lora">Helaba!</h3>',
				html: 'Jij vindt het misschien grappig om onze playlist aan te passen, maar wij vinden dat minder leuk. Als je suggesties hebt voor een betere playlist mag je ons dat altijd laten weten via de <a onclick="swal.closeModal();" href="#/contact">contactpagina</a>.',
				type: 'error',
				showCancelButton: false,
				confirmButtonText: 'Ok, ik begrijp het'
				}).then(function () {
					window.location.href = "#/";
				}, function() {
					window.location.href = "#/";
				});
		});
	}

	function getPlaylist (callback, cb_array) {

		playlistSvc.getNewestPlaylist().then(function (data) {

			vm.current_playlist = data.data.playlist;
			vm.songs_inside_pl = data.data.playlist.songs_inside;
			vm.songs_outside_pl = data.data.playlist.songs_outside;
			pl_breaks = data.data.playlist.breaks;



			if(vm.songs_inside_pl.length == 0) {
				playlist_is_empty = true;
			} else {
				playlist_is_empty = false;
			}
			/*cb_arr = deactivateloader + swalMessage */

			if(callback) {
				if(cb_array && cb_array.length > 0) {

					cb_array.unshift(enterBreaks, calculateTime, callback);

					sortSongs(sortPlaylist, cb_array);
				} else {
					cb_array = [enterBreaks, calculateTime, callback];
					sortSongs(sortPlaylist, cb_array);
				}
			} else {
				cb_array = [enterBreaks, calculateTime];
				sortSongs(sortPlaylist, cb_array);
			}
			

			var findLoadingbar = setInterval(function () {

				if($(".loading-bar")) {

					$(".loading-bar").animate({"width": "100vw"}, 50, function() {
						
						$(".loader").fadeOut();
						$(page).fadeIn();
					});

					clearInterval(findLoadingbar);
				}

			}, 50)

		}, function (error) {
			console.log(error);
			swal("<h2 class='Lora'>Woopsie</h2>", "Het wil niet werken vandaag. Misschien nog eens proberen?", "error");
		});
	}


	function sortBySongTitle (a, b) {

	    var nameA=a.title.toLowerCase(); 
	    var nameB=b.title.toLowerCase();

	    if (nameA < nameB) {
	        return -1; 
	    }
	    if (nameA > nameB) {
	        return 1;
	    }
	    return 0;
	} 

	function sortByPosition (a, b) {
		var number1=a.position;
		var number2=b.position;

		if (number1 < number2) {
	        return -1; 
	    }
	    if (number1 > number2) {
	        return 1;
	    }
	    return 0;

	}

	function sortSongs(callback, cb_array) { 

		vm.songs_outside_pl.sort(sortBySongTitle); 
		
		checkCallback(callback, cb_array);
	}

	function sortPlaylist(callback, cb_array) { 
		
		vm.songs_inside_pl.sort(sortByPosition); 

		checkCallback(callback, cb_array);
	}

	function enterBreaks (callback, cb_array) {

		$(".break").remove();

		var myinterval = setInterval(function () {

			if($(".container table tbody tr:nth-child("+ vm.songs_inside_pl.length+")")) {
				
				clearInterval(myinterval);

				for(i in pl_breaks) {

					if(!pl_breaks[i].temp_removed || pl_breaks[i].temp_removed == false) {

						var current_break = pl_breaks[i];
						var position = parseInt(current_break.after_position) + parseInt(i);

						if($(".container table tbody tr:nth-child("+ position +")")) {
							$("<tr class='grey lighten-2 break' ng-swipe-left='playlist.swipeBreak(" + current_break.id + ")''><td></td><td>PAUZE</td><td>" + current_break.length + "</td><td class='center-align hide-on-small'><i title='Verwijder pauze' class='break_"+current_break.id+" fa fa-minus-circle red-text text-darken-2' ng-click='playlist.removeBreak("+ current_break.id + ")'></i></td></tr>").insertAfter(".container table tbody tr:nth-child("+ position +")");

							
							$compile($('.break'))($scope);
							$compile($('.break_'+current_break.id))($scope);
							
						}
					}
					
				}
			}

			setTimeout(function() { clearInterval(myinterval) }, 2000);

		}, 50);

		checkCallback(callback, cb_array);
	}

	function calculateTime (callback, cb_array) {

		var playlist = vm.songs_inside_pl;
		var totalhours = 00;
		var totalminutes = 0;
		var totalseconds = 0;
		
		for(i in playlist) {
			var minutes = Number(playlist[i].length.slice(0, 2));
			var seconds = Number(playlist[i].length.slice(3, 5));
			totalminutes += minutes;
			totalseconds += seconds;
		}

		for(i in pl_breaks) {
			console.log(pl_breaks);
			if(pl_breaks[i].temp_removed == null || pl_breaks[i].temp_removed == false)
			{
				var minutes = Number(pl_breaks[i].length.slice(0,2));
				totalminutes += minutes;
			}
		}

		var extra_minutes = Math.floor(totalseconds / 60);
		totalminutes += extra_minutes;

		if(totalminutes > 59) {
			totalhours = Math.floor(totalminutes / 60);
			totalminutes = (totalminutes % 60);
		}
		
		totalseconds = totalseconds % 60;


		if(totalhours < 10) {
			totalhours = "0" + totalhours;
		}
		if(totalminutes < 10) {
			totalminutes = "0" + totalminutes;
		}
		if(totalseconds < 10) {
			totalseconds = "0" + totalseconds;
		}


		vm.playlist_time = totalhours + ":" + totalminutes + ":" + totalseconds;

		checkCallback(callback, cb_array);

	}

	function getPlaylists (callback, cb_array) {

		playlistSvc.getPlaylists().then(function (data) {
			vm.allplaylists = data.data.playlists;

			checkCallback(callback, cb_array);

		}, function (error) {	
			console.log(error);
			swal("<h2 class='Lora'>Miserie, miserie</h2>", "Problemen met de website :-(", "error");
		});

	}

	function swalMessage() {

		title = "<h2 class='Lora'>" + swalmessage[0] + "</h2>"

		if(swalmessage[2]) {
			swal(title, swalmessage[1], swalmessage[2]);
		} else {
			swal(title, swalmessage[1]);
		}
		
	} 

	function activateLoader () {

		$(".preloader-bg").fadeIn("fast");
		$("body").css("overflow", "hidden");

	}

	function deactivateLoader (callback, cb_array) {

		$("body").css("overflow", "auto");
		$(".preloader-bg").fadeOut("fast");

		checkCallback(callback, cb_array);
	}

	function checkCallback(cb, cb_arr) {

		if(cb) { 

			if(cb_arr && cb_arr.length > 0) {
				var newcb = cb_arr[0];
				cb_arr.shift();

				cb(newcb, cb_arr);
			} else {
				cb(); 
			}
		}
	}

	function swalCheckIfSaved (myfunction) {
		var alert = swal({
	    	title: '<h3 class="Lora">Wacht even!</h3>',
	    	html:'Je hebt de playlist toch wel opgeslagen?',
	    	confirmButtonText: 'Jup, alles in orde.',
			cancelButtonText: 'Oepsie, vergeten...',
			showCancelButton: true
	    });

	    if(myfunction) {
	    	alert.then(myfunction);
	    }
	}

	function removeAllBreaks () {

		playlistSvc.removeAllBreaks(vm.current_playlist.id).then(function (data) {
	
		});
	}

	vm.showInfoBox = function (myindex, max) {

		var inputOptions = {
			'top': 'Zet bovenaan',
			'bottom': 'Zet onderaan',
			'position': 'Zet op een bepaalde positie'
		}

		if(vm.songs_inside_pl.length > 0) {
			swal({
				title: '<h3 class="Lora">Waar wil je hem hebben?</h3>',
				input: 'radio',
				inputOptions: inputOptions,
				inputValidator: function (result) {
					return new Promise(function (resolve, reject) {

						$scope.$apply(function () {

							var songs = vm.songs_outside_pl;
							var ourplaylist = vm.songs_inside_pl;

							var song = songs[myindex];

							playlistHasChanged = true;

							if (result) {
								if(result == "top") {
									ourplaylist.unshift(song);
									songs.splice(myindex, 1);
									calculateTime();
									resolve();
								} else if (result == 'bottom') {
									ourplaylist.push(song);
									songs.splice(myindex, 1);
									calculateTime();
									resolve();
								} else {
									vm.enterPosition(myindex, max);
								}

								for(i in pl_breaks) {
									
									if(ourplaylist.length >= pl_breaks[i].after_position) {
										pl_breaks[i].temp_removed = false;
									}
								}

								enterBreaks();
								
							} else {
								reject('Je hebt helemaal niets aangeduid...')
							}
						});
					});
				}
			});
		} else {
			vm.songs_inside_pl.push(vm.songs_outside_pl[myindex]);
			vm.songs_outside_pl.splice(myindex, 1);
			calculateTime();
		}

		
	}

	vm.enterPosition = function (myindex, max) {
		swal({
			title: '<h3 class="Lora">Waar wil je hem hebben?</h3>',
			html: '<p>Zet hem op plaats <input type="number" min="1" max="' + max + '" data-myindex="' + myindex + '" class="mar-left-m input-position"/></p>',
			confirmButtonText: 'Verplaatsen maar',
			cancelButtonText: 'Nope, laat maar',
			showCancelButton: 'true',
			type: 'question',
			preConfirm: function() {
				return new Promise(function (resolve, reject) {

					$scope.$apply(function () {
						var input = $(".input-position");
						var position = input[0].value;
						var index = input[0].dataset.myindex;
						
						if(position < 1 || position > vm.songs_inside_pl.length+1) {
							reject("Geef een positie in van 1 t.e.m. " + max);
						} else {
							vm.addToPlaylist(position, index);	
							resolve();
						}
					});
					
				});
			}
		});
	}

	vm.addToPlaylist = function (positie, index) {
		
		var songs = vm.songs_outside_pl;
		var ourplaylist = vm.songs_inside_pl;

		var song = songs[index];

		ourplaylist.splice(positie-1, 0, song);
		songs.splice(index, 1);

		playlistHasChanged = true;

		enterBreaks();

		calculateTime();
		
	}

	vm.removeFromPlaylist = function (index) {
		vm.songs_outside_pl.push(vm.songs_inside_pl[index]);
		vm.songs_inside_pl.splice(index, 1);

		$.when($(".break").remove()).then(enterBreaks());

		if(vm.songs_inside_pl.length == 0) {
			$(".break").remove();
			pl_breaks = [];
			calculateTime();
		}

		for(i in pl_breaks) {
			if(vm.songs_inside_pl.length < pl_breaks[i].after_position) {
				pl_breaks[i].temp_removed = true;
			} 
		}

		sortSongs();
		calculateTime();

		playlistHasChanged = true;

	}

	vm.addBreak = function () {

		swal({
			title:"<h3 class='Lora'>Maak nieuwe pauze</h3>",
			html: "<p>Duur: <input class='input_minutes' type='number'/> minuten <br> Na het <input class='input_break_pos' type='number' min='1' max='"+vm.songs_inside_pl.length+"'/><sup>e</sup> nummer </p>",
			confirmButtonText: "Maak pauze aan",
			cancelButtonText: "Laat die pauze maar",
			showCancelButton: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					var minutes = $(".input_minutes")[0].value;
					var break_pos = $(".input_break_pos")[0].value;

					if(vm.songs_inside_pl.length == 0) {
						swal("<h2 class='Lora'>Luierik!</h2>", "Voeg eerst minstens één nummer toe aan je playlist. Daarna mag je pauzeren.", "error");
						

					} else if (minutes < 1) {
						swal("<h2 class='Lora'>Pestkop!</h2>", "Noem jij dat een pauze? Geef als lengte minsten één minuut in.", "error").then(function () {
							vm.addBreak();
						});
					} else if(break_pos > 0 && break_pos <= vm.songs_inside_pl.length) {

						if(minutes < 10) {
							minutes = "0"+minutes;
						}
						var newbreak = {
							position: break_pos, 
							minutes: minutes +":00",
							playlist: vm.current_playlist.id				
						}


						function add_break () {
 
							playlistSvc.addBreak(newbreak).then(function () {
								swal("<h2 class='Lora'>Gelukt!</h2>", "De pauze werd toegevoegd", "success");
								vm.getPlaylistById(vm.current_playlist.id);

							}, function (error) {
								console.log(error);
								swal("<h2 class='Lora'>Damn!</h2>", "Er ging hier iets mis. Mijn schuld...", "error");
							});
						}

						vm.savePlaylist(add_break);



					} else {
						reject("Geef een positie in van 1 t.e.m. " + vm.songs_inside_pl.length);
					}
				});

			}
		})
	}

	vm.removeBreak = function (break_id) {

		swal({
			title: "<h2 class='Lora'>Zeker?</h2>",
			html: "We hebben allemaal wel eens nood aan een pauze. Zeker dat je deze wil verwijderen?",
			type: "question",
			confirmButtonText: "Geen pauze nodig!",
			cancelButtonText: "Toch liefst een pauze",
			showCancelButton: true
		}).then(function () {

			activateLoader();

			var mybreak = ".break_" + break_id;
			playlistSvc.removeBreak(break_id).then(function (data) {
				$(mybreak).remove();
				swal("<h2 class='Lora'>Vaarwel</h2>", "Daar gaat onze pauze...", "success");
				vm.getPlaylistById(vm.current_playlist.id);
			}, function (error) {
				console.log(error);
				swal("<h2 class='Lora'>Alleei!</h2>", "De pauze wil niet weggaan. Een teken misschien?", "error");
			});
		});

		
	}

	vm.savePlaylist = function (callback) {

		activateLoader(); 

		var songs = vm.songs_outside_pl;
		vm.current_playlist.songs_inside = vm.songs_inside_pl;
		var playlist = vm.current_playlist;

		playlist.songs_inside.forEach(function addPostion (item, index) { item.position = index + 1; });

		for(i in pl_breaks) {
			var thisbreak = pl_breaks[i];
			if(vm.songs_inside_pl.length < thisbreak.after_position) {
				playlistSvc.removeBreak(thisbreak.id).then(function (data) {
					pl_breaks.splice(i, 1);
					calculateTime();
				})
			} 
		}

		pl_breaks = playlist.breaks;

		playlistSvc.savePlaylist(songs, playlist).then(function (data) {

			playlistHasChanged = false;

			if(vm.songs_inside_pl.length > 0) {
				playlist_is_empty = false;
				calculateTime();
			} else {
				removeAllBreaks();
				pl_breaks = [];
				calculateTime();
			}

			swalmessage = ["Gelukt!", "De playlist werd opgeslagen", "success"];
			
			if(callback) {
				deactivateLoader(callback);
			} else {
				deactivateLoader(swalMessage);
			}
			


		}, function (error) {

			swal("<h2 class='Lora'>Oei...</h2>", "Er was een probleem bij het opslagen van de playlist", "error");
			deactivateLoader(swalMessage);
		});

	}

	vm.emptyPlaylist = function () {

		swal({
			title: '<h2 class="Lora">Zeker?</h2>',
			text: 'Weet je zeker dat je de playlist wil leegmaken?',
			confirmButtonText: 'Jaja, leegmaken die handel!',
			showCancelButton: true,
			cancelButtonText: 'Toch maar niet.',
			type:'question'
		}).then(function () {
			activateLoader();

			playlistSvc.emptyPlaylist(vm.current_playlist.id).then(function (data) {
				
				vm.songs_inside_pl = [];
				pl_breaks = [];
				sortSongs();

				playlistHasChanged = false;

				var cb_arr = [swalMessage];

				swalmessage = ["Weg ermee!", "De playlist is leeg gemaakt!", "success"];
				calculateTime(deactivateLoader,cb_arr);
			}, function (error) {
				console.log(error);
				swal("<h2 class='Lora'>Huh?</h2>", "De nummers zijn er nog steeds...", "error");
			});
		}, function () {
			swal('<h2 class="Lora">Niet dan</h2>',
				 'Geen zorgen, de nummers staan er nog in.',
			)
		})
		
	}

	vm.toggleSideBar = function () {

		$('ul.tabs').tabs();

		if(showSidebar) {

			$(".playlistpage > .container > div:nth-child(2) > div").removeClass("shiftdiv");

			$(".tab-container i").removeClass("fa-caret-left").addClass("fa-caret-right");

			$(".tab-container").animate({
		       left: '-5px'
		    }, { duration: 200, queue: false });

			$(".sidebox").animate({
		       left: '-445px'
		    }, { duration: 200, queue: false });

		} else {

			$(".playlistpage > .container > div:nth-child(2) > div").addClass("shiftdiv");

			$(".tab-container i").removeClass("fa-caret-right").addClass("fa-caret-left");

			$(".tab-container").animate({
		       left: '440px'
		    }, { duration: 200, queue: false });

			$(".sidebox").animate({
		       left: '0px'
		    }, { duration: 200, queue: false });
		}

		showSidebar = !showSidebar;

	}

	vm.swipeSidebar = function (bl_show) {

		$('ul.tabs').tabs();

		if($(window).width() <= 768) {
			if(bl_show) {
				$("body").css("overflow", "hidden");
				$(".playlistpage .sidebox").addClass("showsidebox");
			} else {
				$("body").css("overflow-y", "auto");
				$(".playlistpage .sidebox").removeClass("showsidebox");
			}
		}
	}

	vm.swipeSong = function (index) {
		if($(window).width() <= 414) {
			vm.removeFromPlaylist(index);
		}
	}

	vm.swipeBreak = function (break_id) {
		if($(window).width() <= 414) {
			vm.removeBreak(break_id);
		}
	}

	vm.setSelected = function (tab) {
		vm.selected = tab;
	}

	vm.isSelected = function (tab) {
		return vm.selected === tab;
	}

	vm.deletePlaylist = function (id) {

		swal({
			title: '<h2 class="Lora">Zeker?</h2>',
			text: 'Weet je zeker dat je de playlist wil verwijderen?',
			confirmButtonText: 'Jaja, verwijderen maar!',
			showCancelButton: true,
			cancelButtonText: 'Toch maar niet.',
			type:'question'
		}).then(function () {

			activateLoader();
			playlistSvc.deletePlaylist(id).then(function (data) {

				swalmessage = ["Weg ermee!", "De playlist is verwijderd!", "success"];
				
				var cb_array = [deactivateLoader, swalMessage];

				getPlaylists(getPlaylist, cb_array);
				
			}, function (error) {
				console.log(error);
				swal("<h2 class='Lora'>Helaas</h2>", "Deze playlist weigert te gaan...", "error");
			});

		}, function () {
			swal('<h2 class="Lora">Niet dan</h2>',
				 'Geen zorgen, je playlist is veilig!',
			)
		});
	}

	vm.getPlaylistById = function (id) {

		var doIt = function () {

			playlistHasChanged = false;
			activateLoader();

			playlistSvc.getPlaylistById(id).then(function (data) {

				vm.current_playlist = data.data.playlist;
				vm.songs_inside_pl = data.data.playlist.songs_inside;
				vm.songs_outside_pl = data.data.playlist.songs_outside;
				pl_breaks = data.data.playlist.breaks;

				var cb_array = [enterBreaks, calculateTime, deactivateLoader];

				sortSongs(sortPlaylist, cb_array);
				

			}, function (error) {
				console.log(error);
				swal("<h2 class='Lora'>Mo seg!</h2>", "Ik vind deze playlist precies niet...", "error");
			});
		}

		if(playlistHasChanged) {
			swalCheckIfSaved(doIt);
		} else {
			doIt();
		}
	}

	vm.addNewPlaylist = function () {

		swal({
			title: '<h3 class="Lora">Geef een naam in</h3>',
			input: 'text',
			inputClass: 'input_pl_name',
			confirmButtonText: 'Maak de playlist aan',
			cancelButtonText: 'Nope, laat maar',
			showCancelButton: true,
			type: 'info',
			preConfirm: function(pl_name) {

				return new Promise(function (resolve, reject) {
					activateLoader();
					playlistSvc.createPlaylist({name: pl_name}).then(function (data) {
						resolve();
					}, function (error) {
						reject("Door problemen konden we de playlist niet aanmaken");
					});
				})
			}
		}).then(function (name) {

			var cb_arr = [deactivateLoader, swalMessage];

			swalmessage = ["Gelukt!", "De playlist " + name + " is aangemaakt!", "success"];

			getPlaylists(getPlaylist, cb_arr);
				
		});

	}

	function _init() {
		$(page).hide();
		toggleLoadingSign(false);
		checkToken(getPlaylist, [getPlaylists]);
	}

	_init();

	

	window.onbeforeunload = function(){
		if($state.current.name == "playlist") {
		
			return '';

		}
	};

	$scope.$on('$stateChangeStart', function (event, o_toState) {
		
		if(isAuthorized && playlistHasChanged && !changeState) {
			event.preventDefault();

			swalCheckIfSaved(function() {
		    	changeState = true;
		    	$state.go(o_toState.name);
		    });
		}
		
	});

});