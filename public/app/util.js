function handleLoading (page, src_array) {

	$(page).hide();
	var queue = new createjs.LoadQueue();
	queue.on("progress", handleProgress, this);
	queue.loadManifest(src_array);



	function handleProgress (e) {

		var widthpercentage = Math.round(e.progress*100);

		var findLoadingbar = setInterval(function () {

			if($(".loading-bar")) {
				$(".loading-bar").animate({"width": widthpercentage + "vw"}, 25);

				if(e.progress == 1) {
					$(".loading-bar").animate({"width": "100vw"}, 50, function() {
						
						$(".loader").fadeOut();
						$(page).fadeIn();
					});

				}

				clearInterval(findLoadingbar);
			}

		}, 50)
		
	}

}

