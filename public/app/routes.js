app.config(function ($stateProvider, $urlRouterProvider) {
 
  $urlRouterProvider.otherwise("/");

  $stateProvider
	.state("home", {
	  url: "/",
	  templateUrl: "app/pages/home.html",
	  controller: "homeController as home",
	  onEnter: goToTop
	})
	.state("events", {
	  url: "/evenementen",
	  templateUrl: "app/pages/events.html",
	  controller: "eventsController as event",
	  onEnter: goToTop
	})
	.state("unsubscribe", {
	  url: "/unsubscribe/:token",
	  templateUrl: "app/pages/unsubscribe.html",
	  controller: "unsubscribeController as unsubscribe",
	  onEnter: goToTop
	})
	.state("music", {
	  url: "/muziek",
	  templateUrl: "app/pages/music.html",
	  controller: "musicController as music",
	  onEnter: goToTop
	})
	.state("playlist", {
	  url: "/playlist/:token/edit",
	  templateUrl: "app/pages/playlist.html",
	  controller: "playlistController as playlist",
	  onEnter: goToTop
	})
	.state("gallery", {
	  url: "/foto's",
	  templateUrl: "app/pages/gallery.html",
	  controller: "galleryController as gallery",
	  onEnter: goToTop
	})
	.state("about", {
	  url: "/over",
	  templateUrl: "app/pages/about.html",
	  controller: "aboutController as about",
	  onEnter: goToTop
	})
	.state("contact", {
	  url: "/contact",
	  templateUrl: "app/pages/contact.html",
	  controller: "contactController as contact",
	  onEnter: goToTop
	})

});

var goToTop = function()
{
    window.scrollTo(0,0);
}

