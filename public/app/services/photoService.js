app.service('photoService', function($http) {

	var svc = this;

	svc.getPhotosByPage = function (page) {

		return $http.get('/api/photos/' + page);
	}
});