	app.service('emailService', function($http) {

	var svc = this;

	svc.getRegisteredEmails = function () {

		return $http.get('/api/emails');
	}

	svc.NewRegisteredEmail = function (data) {

		return $http.post('/api/email/new', data);
	}

	svc.deleteRegisteredEmails = function (token) {

		var req = {
			method: "post",
			url: '/api/email/delete/' + token,
			headers: {"X-HTTP-Method-Override": "DELETE"}
		};

		return $http(req);
	}

	svc.checkToken = function (token) {
		return $http.get('/api/email/' + token);
	}
});