app.service('playlistService', function($http) {

	var svc = this;

	svc.getSongs = function () {

		return $http.get('/api/songs');
	}

	svc.getPlaylists = function () {

		return $http.get('/api/playlists');
	}

	svc.getNewestPlaylist = function () {

		return $http.get('/api/playlist/newest');
	}

	svc.getPlaylistById = function (id) {

		return $http.get('/api/playlist/' + id);
	}

	svc.addBreak = function (mybreak) {

		return $http.post('api/playlist/break/new', mybreak);
	}

	svc.removeBreak = function (id) {

		return $http.get('api/playlist/break/delete/' + id);
	}

	svc.removeAllBreaks = function (id) {

		return $http.get('api/playlist/breaks/delete/' + id);
	}

	svc.savePlaylist = function (songs, playlist) {

		var object = {
			songs: songs,
			playlist: playlist
		};

		return $http.post('/api/playlist/save', object);
	}

	svc.emptyPlaylist = function (id) {
		return $http.get('/api/playlist/reset/' + id);
	}

	svc.deletePlaylist = function (id) {
		return $http.get('/api/playlist/delete/' + id);
	}

	svc.createPlaylist = function (data) {
		console.log(data);
		return $http.post('/api/playlist/create', data);
	}
});