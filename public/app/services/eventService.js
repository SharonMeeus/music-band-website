app.service('eventService', function($http) {

	var svc = this;

	svc.getLatestEvent = function () {

		return $http.get('/api/latestevent');
	}

	svc.newLatestEvent = function (data) {

		return $http.post('/api/latestevent/new', data);
	}
});