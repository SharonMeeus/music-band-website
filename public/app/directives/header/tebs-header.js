app.directive("tebsHeader", function () {
	return {
		restrict: "E", 
		templateUrl: "app/directives/header/tebs-header.html",
		replace: true,
		scope: {},
		bindToController: true,
		controllerAs: "head",
		controller: function () {

		},

		link: function (scope, elems, attr) {
			
			$(".dropdown-button").dropdown();
			// JQuery Side Nav
			$(".button-collapse").sideNav({
				closeOnClick: true
			});
		}

	}
});