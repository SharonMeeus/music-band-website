app.directive('magnificPopup', function() {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function() {
            $('.img-container').magnificPopup({
            	delegate:'a',
            	type:'image',
            	gallery: {enabled: true},
            	mainClass: "mfp-with-zoom",
            	zoom: {
    			    enabled: true,

    			    duration: 300, 
    			    easing: 'ease-in-out',
    			}

            });

            $('.mfp-youtube').magnificPopup({
                type:'iframe',
                gallery: {enabled: true}
            
            
            });
        }
    };
});