<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Songposition;

class SongpositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();

        DB::table('songpositions')->delete();

    	$songs = array(
    		[
    			"position" => 1,
    			"playlist_id" => 1,
    			"playlistsong_id" => 1
    		],
    		[
    			"position" => 2,
    			"playlist_id" => 1,
    			"playlistsong_id" => 28
    		],
    		[
    			"position" => 3,
    			"playlist_id" => 1,
    			"playlistsong_id" => 5
    		],
    		[
    			"position" => 4,
    			"playlist_id" => 1,
    			"playlistsong_id" => 6
    		],
    		[
    			"position" => 5,
    			"playlist_id" => 1,
    			"playlistsong_id" => 25
    		],
    		[
    			"position" => 6,
    			"playlist_id" => 1,
    			"playlistsong_id" => 31
    		],
    		[
    			"position" => 7,
    			"playlist_id" => 1,
    			"playlistsong_id" => 26
    		],
    		[
    			"position" => 8,
    			"playlist_id" => 1,
    			"playlistsong_id" => 8
    		],
    		[
    			"position" => 9,
    			"playlist_id" => 1,
    			"playlistsong_id" => 32
    		],
    		[
    			"position" => 10,
    			"playlist_id" => 1,
    			"playlistsong_id" => 22
    		],
    		[
    			"position" => 11,
    			"playlist_id" => 1,
    			"playlistsong_id" => 12
    		],
    		[
    			"position" => 12,
    			"playlist_id" => 1,
    			"playlistsong_id" => 24
    		],
    		[
    			"position" => 13,
    			"playlist_id" => 1,
    			"playlistsong_id" => 14
    		],
    		[
    			"position" => 14,
    			"playlist_id" => 1,
    			"playlistsong_id" => 15
    		],
    		[
    			"position" => 15,
    			"playlist_id" => 1,
    			"playlistsong_id" => 16
    		],
    		[
    			"position" => 16,
    			"playlist_id" => 1,
    			"playlistsong_id" => 17
    		],
    		[
    			"position" => 17,
    			"playlist_id" => 1,
    			"playlistsong_id" => 18
    		],
    		[
    			"position" => 18,
    			"playlist_id" => 1,
    			"playlistsong_id" => 21
    		],
    		[
    			"position" => 19,
    			"playlist_id" => 1,
    			"playlistsong_id" => 19
    		],
    		[
    			"position" => 20,
    			"playlist_id" => 1,
    			"playlistsong_id" => 20
    		],
    		[
    			"position" => 21,
    			"playlist_id" => 1,
    			"playlistsong_id" => 41
    		],

    	);
		
		foreach ($songs as $song)
        {
            Songposition::create($song);
        }
        
        Model::reguard();
    }
}
