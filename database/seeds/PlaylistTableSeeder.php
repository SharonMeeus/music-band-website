<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Playlist;

class PlaylistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('playlists')->delete();

        $playlists = array(

        	[
        		'name' => 'Ginderbroek Kermis'
        	],

        );

        foreach ($playlists as $playlist)
        {
            Playlist::create($playlist);
        }
        
        Model::reguard();
    }
}
