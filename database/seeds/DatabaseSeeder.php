<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call(FacebookNumberTableSeeder::class);
        $this->call(PhotoTableSeeder::class);
        $this->call(LatestEventTableSeeder::class);*/
        $this->call(PlaylistSongTableSeeder::class);
        $this->call(PlaylistTableSeeder::class);
        $this->call(SongpositionTableSeeder::class);
    }
}
