<?php

use Illuminate\Database\Seeder;

class FacebookNumberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facebook_numbers')->delete();

    	DB::table('facebook_numbers')->insert([
    		'app_id' => '1908190316060720',
    		'app_secret' => '033635a28c0a95dd44154b4db61caec2'
    	]);
    }
}
