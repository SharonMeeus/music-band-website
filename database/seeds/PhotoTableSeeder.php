<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Photo;

class PhotoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();

        DB::table('photos')->delete();

    	$photos = array(
    		[
    			'src' => 'img/about/band.jpg',
    			'alt' => 'Groepsfoto van The Early Bird Specials',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/bart.jpg',
    			'alt' => 'Foto van Bart, onze saxofonist',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/franky.jpg',
    			'alt' => 'Foto van Franky, onze drummer',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/geert.jpg',
    			'alt' => 'Foto van Geert, onze bassist',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/isabelle.jpg',
    			'alt' => 'Foto van Isabelle, onze zangeres',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/kristof.jpg',
    			'alt' => 'Foto van Kristof, onze gitarist',
    			'page' => 'about'
    		],
    		[
    			'src' => 'img/about/sharon.jpg',
    			'alt' => 'Foto van Sharon, onze zangeres en toetseniste',
    			'page' => 'about'
    		],



    		[
    			'src' => 'img/contact/img5.jpg',
    			'alt' => '',
    			'page' => 'contact'
    		],
    		[
    			'src' => 'img/home/BG.jpg',
    			'alt' => '',
    			'page' => 'home'
    		],

    		[
    			'src' => 'img/photo\'s/img1.jpg',
    			'alt' => 'Sharon aan het zingen tijdens een optreden bij Jazzoet met Geert in de achtergrond.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img2.jpg',
    			'alt' => 'Bart en Franky die lachend naar elkaar kijken.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img3.jpg',
    			'alt' => 'Franky achter zijn drumstel op het podium van Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img4.jpg',
    			'alt' => 'Het publiek dat mee klapt en juicht tijdens een optreden bij Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img5.jpg',
    			'alt' => 'Isabelle op het podium van Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img6.jpg',
    			'alt' => 'Bart die saxofoon speelt tijdens een optreden bij Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img7.jpg',
    			'alt' => 'Geert die bass speelt bij Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img8.jpg',
    			'alt' => 'Sharon die keys speelt bij Jazzoet.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img9.jpg',
    			'alt' => 'Geert die glimlachend in zijn handen klapt.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img10.jpg',
    			'alt' => 'Isabelle die aan het zingen is tijdens een buurtfeest in Overpelt.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img11.jpg',
    			'alt' => 'Bart die saxofoon speelt tijdens een buurtfeest in Overpelt.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img12.jpg',
    			'alt' => 'Franky strijkt lachend met een van zijn brushes over Bart zijn voorhoofd.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img13.jpg',
    			'alt' => 'T.E.B.S. zit vrolijk rond de tafel tijdens het bespreken van hun nummers.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img14.jpg',
    			'alt' => 'Bart speelt live saxofoon tijdens een zelf georganiseerd optreden.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img15.jpg',
    			'alt' => 'Isabelle, Sharon en Kristof bekijken en bespreken een partituur.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img16.jpg',
    			'alt' => 'Geert, Franky en Bart bekijken een partituur.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img17.jpg',
    			'alt' => 'T.E.B.S. live tijdens een zelf georganiseerd optreden.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img18.jpg',
    			'alt' => 'Sharon zingt en speelt keys, met Bart die saxofoon speelt en Isabelle die de beatring gebruikt.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img19.jpg',
    			'alt' => 'Franky achter zijn drumstel tijdens een zelf georganiseerd optreden.',
    			'page' => 'photo\'s'
    		],
    		[
    			'src' => 'img/photo\'s/img20.jpg',
    			'alt' => 'Geert en Kristof live aan het spelen tijdens een zelf georganiseerd optreden.',
    			'page' => 'photo\'s'
    		],


    	);

		foreach ($photos as $photo)
        {
            Photo::create($photo);
        }
        
        Model::reguard();
    }
}
