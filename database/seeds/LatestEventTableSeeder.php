<?php

use Illuminate\Database\Seeder;

class LatestEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('latest_events')->delete();

    	DB::table('latest_events')->insert([
    		'title' => 'Ginderbroek Kermis',
    		'date' => '2017-06-10 20:00:00' 
    	]);
    }
}
