<?php

use Illuminate\Database\Seeder;

class PlaylistSongTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('playlist_songs')->delete();

        $songs = array([
    			'artist' => 'Caro Emerald',
    			'title' => 'A Night Like This',
    			'youtube_url' => 'https://www.youtube.com/watch?v=74LXx0wSqMI',
                'length' => '03:45'
    		],
    		[
    			'artist' => 'Eliza Doolittle',
    			'title' => 'Pack Up',
    			'youtube_url' => 'https://www.youtube.com/watch?v=dzY0-I4Gq5w',
                'length' => '00:00'
    		],
    		[
    			'artist' => 'James Morrison',
				'title' => 'You Give Me Something',
				'youtube_url' => "https://www.youtube.com/watch?v=UZp6dhheriM",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Lady Linn',
				'title' => "I Don't Wanna Dance",
				'youtube_url' => "https://www.youtube.com/watch?v=JM8yToraPwg",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Stevie Wonder',
				'title' => "Isn't She Lovely",
				'youtube_url' => "https://www.youtube.com/watch?v=8r92A7ndnZk",
                'length' => '02:51'
    		],
    		[
    			'artist' => 'Caro Emerald',
				'title' => 'Back It Up',
				'youtube_url' => "https://www.youtube.com/watch?v=jo1cyl0QbWo",
                'length' => '04:13'
    		],
    		[
    			'artist' => 'Amy Winehouse',
				'title' => 'Love Is a Losing Game',
				'youtube_url' => "https://www.youtube.com/watch?v=nMO5Ko_77Hk",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Amy Winehouse',
				'title' => 'Rehab',
				'youtube_url' => "https://www.youtube.com/watch?v=KUmZp8pR1uc",
                'length' => '03:33'
    		],
    		[
    			'artist' => 'Duffy',
				'title' => 'Stepping Stone',
				'youtube_url' => "https://www.youtube.com/watch?v=KFjF64Mcaao",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Gerald Marks',
				'title' => 'All Of Me',
				'youtube_url' => "https://www.youtube.com/watch?v=NXKYTJHnqvw",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Eric Clapton',
				'title' => 'Layla',
				'youtube_url' => "https://www.youtube.com/watch?v=pKwQlm-wldA",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Caro Emerald',
				'title' => 'Dream a Little Dream',
				'youtube_url' => "https://www.youtube.com/watch?v=NfykP-yY-dI",
                'length' => '03:28'
    		],
    		[
    			'artist' => 'Adele',
				'title' => 'To Make You Feel My Love',
				'youtube_url' => "https://www.youtube.com/watch?v=0put0_a--Ng",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Eva De Roovere',
				'title' => 'Fantastig Toch',
				'youtube_url' => "https://www.youtube.com/watch?v=F7SXCe8G_3M",
                'length' => '02:44'
    		],
    		[
    			'artist' => 'Of Monsters and Men',
				'title' => 'Little Talks',
				'youtube_url' => "https://www.youtube.com/watch?v=ghb6eDopW8I",
                'length' => '04:30'
    		],
    		[
    			'artist' => 'Duffy',
				'title' => 'Mercy',
				'youtube_url' => "https://www.youtube.com/watch?v=y7ZEVA5dy-Y",
                'length' => '03:23'
    		],
    		[
    			'artist' => 'Adele',
				'title' => 'Rumour Has It',
				'youtube_url' => "https://www.youtube.com/watch?v=uK3MLlTL5Ko",
                'length' => '03:32'
    		],
    		[
    			'artist' => 'Amy Winehouse',
				'title' => 'Valerie',
				'youtube_url' => "https://www.youtube.com/watch?v=aeVUvA_BOzs",
                'length' => '03:20'
    		],
    		[
    			'artist' => 'The Noisettes',
				'title' => 'Never Forget You',
				'youtube_url' => "https://www.youtube.com/watch?v=KRFHiBW9RE8",
                'length' => '04:33'
    		],
    		[
    			'artist' => 'Kings of Leon',
				'title' => 'Use Somebody',
				'youtube_url' => "https://www.youtube.com/watch?v=gnhXHvRoUd0",
                'length' => '03:46'
    		],
    		[
    			'artist' => 'Joan Osborne',
				'title' => 'Heatwave',
				'youtube_url' => "https://www.youtube.com/watch?v=PTKFR3ra2ts",
                'length' => '02:30'
    		],
    		[
    			'artist' => 'Jamie Cullum',
				'title' => "Don't Stop the Music",
				'youtube_url' => "https://www.youtube.com/watch?v=S0z1Mo7O6dE",
                'length' => '04:42'
    		],
    		[
    			'artist' => 'Plan B',
				'title' => "She Said",
				'youtube_url' => "https://www.youtube.com/watch?v=rQjh9H-ymK4",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Meghan Trainor',
				'title' => "All About That Bass",
				'youtube_url' => "https://www.youtube.com/watch?v=7PCkvCPvDXk",
                'length' => '03:46'
    		],
    		[
    			'artist' => 'Nina Simone',
				'title' => "Feeling Good",
				'youtube_url' => "https://www.youtube.com/watch?v=Ff-0pHwyQ1g",
                'length' => '04:44'
    		],
    		[
    			'artist' => 'Kovacs',
				'title' => "50 Shades of Black",
				'youtube_url' => "https://www.youtube.com/watch?v=9DQl322R12M",
                'length' => '03:35'
    		],
    		[
    			'artist' => 'Radiohead',
				'title' => "Creep",
				'youtube_url' => "https://www.youtube.com/watch?v=XFkzRNyygfk",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Amy Winehouse',
				'title' => "Back to Black",
				'youtube_url' => "https://www.youtube.com/watch?v=TJAfLE39ZZ8",
                'length' => '04:04'
    		],
    		[
    			'artist' => 'Ella Fitzgerald & Louis Armstrong',
				'title' => "Cheek to Cheek",
				'youtube_url' => "https://www.youtube.com/watch?v=a3dvZ2Cv8wI",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Train',
				'title' => "Hey, Soul Sister",
				'youtube_url' => "https://www.youtube.com/watch?v=kVpv8-5XWOI",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Taylor Swift',
				'title' => "Shake It Off",
				'youtube_url' => "https://www.youtube.com/watch?v=nfWlot6h_JM",
                'length' => '03:49'
    		],
    		[
    			'artist' => 'Loïc Nottet',
				'title' => "Rhythm Inside",
				'youtube_url' => "https://www.youtube.com/watch?v=5c_rUBH52C4",
                'length' => '03:38'
    		],
    		[
    			'artist' => 'Diana Krall',
				'title' => "Fly Me to the Moon",
				'youtube_url' => "https://www.youtube.com/watch?v=-b8brVSAAQA",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Amy Macdonald',
				'title' => "This is the Life",
				'youtube_url' => "https://www.youtube.com/watch?v=iRYvuS9OxdA",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Adele',
				'title' => "Rolling In the Deep",
				'youtube_url' => "https://www.youtube.com/watch?v=rYEDA3JcQqw",
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Peggy Lee',
    			'title' => 'Fever',
    			'youtube_url' => 'https://www.youtube.com/watch?v=wyJSc4KlmPA',
                'length' => '00:00'
    		],
    		[
    			'artist' => 'G. Gershwin/Van Morrison',
    			'title' => 'Summertime/Moondance',
    			'youtube_url' => 'https://www.youtube.com/watch?v=v3CwEZrYJ7Q',
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Beth Hart',
    			'title' => 'Chocolate Jesus',
    			'youtube_url' => 'https://www.youtube.com/watch?v=k4lUXWugX_4',
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Lorde',
    			'title' => 'Royals',
    			'youtube_url' => 'https://www.youtube.com/watch?v=nlcIKh6sBtc',
                'length' => '00:00'
    		],
    		[
    			'artist' => 'Stevie Wonder',
    			'title' => 'Superstition',
    			'youtube_url' => 'https://www.youtube.com/watch?v=0CFuCYNx-1g',
                'length' => '00:00'
    		],
            [
                'artist' => 'Take That',
                'title' => 'The Beatles Medley',
                'youtube_url' => 'https://www.youtube.com/watch?v=0CFuCYNx-1g',
                'length' => '10:20'
            ]);
		
		foreach ($songs as $song)
        {
            DB::table('playlist_songs')->insert($song);
        }
    	

    }
}
