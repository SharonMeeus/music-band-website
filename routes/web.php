<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return View::make('index'); // app/views/index.php
});

Route::group(array('prefix' => 'api'), function() {

	Route::get('facebooknumber', 'FacebookNumberController@index');

	Route::get('latestevent', 'LatestEventController@getLatestEvent');
	Route::post('latestevent/new', 'LatestEventController@store');

	Route::get('photos/{page}', 'PhotoController@index');

	Route::get('songs', 'PlaylistSongController@index');

	Route::get('playlists', 'PlaylistController@index');
	Route::get('playlist/newest', 'PlaylistSongController@newestPlaylist');
	Route::get('playlist/{playlist_id}', 'PlaylistSongController@PlaylistById');
	Route::get('playlist/reset/{id}', 'PlaylistSongController@emptyPlaylist');
	Route::get('playlist/delete/{id}', 'PlaylistController@deletePlaylist');
	Route::post('playlist/save', 'PlaylistSongController@savePlaylist');
	Route::post('playlist/create', 'PlaylistController@store');
	Route::post('playlist/break/new', 'InterludeController@store');
	Route::get('playlist/break/delete/{id}', 'InterludeController@destroy');
	Route::get('playlist/breaks/delete/{id}', 'InterludeController@removeBreaksByPlaylist');

	Route::get('emails', 'RegisteredEmailController@index');
	Route::get('email/{token}', 'RegisteredEmailController@checkToken');
	Route::post('email/new', 'RegisteredEmailController@store');
	Route::delete('email/delete/{token}', 'RegisteredEmailController@destroy');

});